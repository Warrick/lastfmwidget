// Last.FM Widget
// Copyright Drew Short (C) 2012
// ALL RIGHTS RESERVED

// Expose the last value returned for debugging
var lastResponse; //the last response from the cache or the Last.FM API servers
var lastFMUser; // The last.FM username that was entered
var lastFMLimit = 10; //10 items requested is the default

var FORMAT = "&format=json"; //everything is requested in JSON from Last.FM to reduce parsing time
var APIKEY = "&api_key=566b24ffeff99a6d541b7cc77a892ddf";
var APISERVER = "http://ws.audioscrobbler.com/2.0/?";
var GLOBALCACHETIMEOUT = 5; //5 minute timout is default

//////////////////////
// Main Menu Object //
//////////////////////

/**
 * This constructs the main menu to be populated by the other function
 * 
 * @param {Object} userName Last.FM username to pull api information from
 * @param {Object} tagid The tagid that the controll is nested under for proper placement
 */
function setupMainMenu(userName, tagid)
{
	//This is the template that the widget will live in and update
	var menuBlock = "<div id='lfmwMenuBlockHeader'>\n"
			+ "<a href='http://www.last.fm/user/" + userName + "' target='_blank'>\n"
			+ "<span id='lfmwMenuUserName'>" + userName + "</span>\n"
			+ "</a>\n"
			+ "<div id='lfmwMenuButtons'></div>\n"
			+ "<div id='lfmwMenuContent'></div>\n"
			+ "<div if='lfmwMenuOptions'></div>\n"
			+ "</div>";
	document.getElementById(tagid).innerHTML = menuBlock;
}

/**
 * This displays the main menu to expose the full functionality of the widget.
 * If you would only like to show a specific page, you can call that specific function
 * 
 * @param {Object} userName Last.FM username to pull api information from
 * @param {Object} tagid The tagid that the controll is nested under for proper placement
 */
function displayMainMenu(userName, tagid, requestedContent)
{
	lastFMUser = userName;
	//inject the html that represents the frame of the widget
	setupMainMenu(userName, tagid);
	//display the buttons for requested content
	var defaultRun = displayMenuAddButtons(requestedContent);
	//run the first button selected as the default
	eval(defaultRun);
}

/**
 * Add all the menu buttons to the widget
 * 
 */
function displayMenuAddButtons(requestedContent)
{
	
	//get all the available buttons
	var availableContentButtons = getAvailableButtons();
	
	//loop through all the available buttons and add them to the list of available buttons
	var availableButtons = "";
	for (index in availableContentButtons)
	{
		availableButtons += index + ",";
	}
	availableButtons = availableButtons.substr(0,availableButtons.length-1);
	
	//the button html to be displayed
	var selectedButtons = "";
	
	//if requestedButtons are not defined use all the available buttons
	requestedContent = requestedContent || availableButtons;
	
	//loop through the selection and add the requested buttons to the html 
	var content = requestedContent.split(",");
	for (index in content)
	{
		var button = availableContentButtons[content[index]];
		var buttonHTML = "<button id='lfmwMenuButton' onclick='" + button["exec"] + "'>" + button["text"] + "</button>\n";
		selectedButtons += buttonHTML;
	}
	
	//insert the buttons in the button menu
	document.getElementById("lfmwMenuButtons").innerHTML = selectedButtons;
	
	//return the first button execute statement
	return availableContentButtons[content[0]]["exec"];
}

/**
 * 
 * 
 * @param {Object} content The html content to display in the content section
 */
function displayContent(content)
{
	//callback to display html in the content block
	document.getElementById("lfmwMenuContent").innerHTML = content;
}

/**
 * Clear the content from the content section
 */
function clearContent()
{
	//clear the content block (this is a debug function)
	document.getElementById("lfmwMenuContent").innerHTML = "";
}

/**
 * Display loading text to let the user know we are working on a getting data 
 */
function displayLoadingContent()
{
	//html to display to indicate that we are loading data
	var loadingHTML = "<table id='lfmwContentTable'>\n"
					+"<tr>\n"
					+ "<td>"
					+ "Please be patient, we are loading data..."
					+ "</td>\n"
					+ "</tr>\n"
					+ "</table>";
	document.getElementById("lfmwMenuContent").innerHTML = loadingHTML;
}

/**
 * Get all the buttons that are available for content 
 */
function getAvailableButtons()
{
	//"exec" is the javascript code that will be executed on a button press
	//"text" is the text to display inside the button
	//Each button must have these or it will not work
	
	//Button for displaying recent tracks
	var displayRecentTracksButton = new Object();
	displayRecentTracksButton["exec"] = "displayRecentTracks(\"" + lastFMUser + "\",\"" + lastFMLimit + "\")";
	displayRecentTracksButton["text"] = "Recent Tracks";
	
	//Button for displaying the top listened to tracks
	var displayTopTracksButton  = new Object();
	displayTopTracksButton["exec"] = "displayTopTracks(\"" + lastFMUser + "\",\"" + 30 + "\")";
	displayTopTracksButton["text"] = "Top Tracks";
	
	//Button for displaying top albums
	var displayTopAlbumsButton  = new Object();
	displayTopAlbumsButton["exec"] = "displayTopAlbums(\"" + lastFMUser + "\",\"" + 30 + "\")";
	displayTopAlbumsButton["text"] = "Top Albums";
	
	//Button for displaying the top listened to artists
	var displayTopArtistsButton  = new Object();
	displayTopArtistsButton["exec"] = "displayTopArtists(\"" + lastFMUser + "\",\"" + 30 + "\")";
	displayTopArtistsButton["text"] = "Top Artists";
	
	//Button for displaying recently loved songs
	var displayRecentlyLovedButton  = new Object();
	displayRecentlyLovedButton["exec"] = "displayRecentlyLoved(\"" + lastFMUser + "\",\"" + lastFMLimit + "\")";
	displayRecentlyLovedButton["text"] = "Recently Loved";

	// Represents all the available buttons //
	var availableContentButtons = new Object();
	availableContentButtons["rt"] = displayRecentTracksButton;
	availableContentButtons["tt"] = displayTopTracksButton;
	availableContentButtons["ta"] = displayTopAlbumsButton;
	availableContentButtons["tr"] = displayTopArtistsButton;
	availableContentButtons["rl"] = displayRecentlyLovedButton;
	
	return availableContentButtons;
}

/////////////////////
// Generic Methods //
/////////////////////

/**
 * Make a request to a URL and process it as an AJAX.
 * 
 * @param {Object} url the url to call
 * @param {Object} ajaxFunction Function to execute when the ajax completes 
 * @param {Object} params The Params object that will be passed to the ajaxFunction
 * @param {Object} cacheTimeout Optional cacheTimout paramaeter, will default to 5 mins if not provied
 */
function request(url, ajaxFunction, params, cacheTimeout)
{
	//check the cache to see if it is their and expired
	if (!cacheExpired(url, cacheTimeout))
	{
		 var response = getCache(url)["cachedItem"];
		 params["response"] = response;
		 lastResponse = response;
		 ajaxFunction(params);
	}
	//otherwise perform the request from the last.fm api servers
	else
	{
		//xmlhttp request object
		var xrequest;
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
		  	xrequest = new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
		  	xrequest = new ActiveXObject("Microsoft.XMLHTTP");
		}
		//function to execute when we get a response
		xrequest.onreadystatechange=function()
	  	{
	  		//if the request is good we'll add it to the cache and process it with our AJAX callback function'
	  		if (xrequest.readyState==4 && xrequest.status==200)
	    	{
	    		//parse the response
	    		var response = JSON.parse(xrequest.responseText);
	    		//record the last response as this one
	    		lastResponse = response;
	    		//store the response in the cache
	    		storeCache(url, response);
	    		//add the response to the parameters to pass to the AJAX callback function
	    		params["response"] = response;
	    		//execute the AJAX callback function
	    		ajaxFunction(params);
	    	}
		}
		//make the request
		xrequest.open("GET",url,true);
		xrequest.send();
	}
}

/**
 * Build a last.fm api url request from parameters
 * 
 * @param {Object} method The last.fm method that is going to be called
 * @param {Object} urlParams the URL paramters to add 
 */
function buildRequestURL(method, urlParams)
{
	//create a base URL with the method
	var apiurl = APISERVER + "method=" + method;
	//loop through each parameter to add
	for (param in urlParams)
	{
		apiurl += "&" + param + "=" + urlParams[param];
	}
	//finish with the apikey and the format
	apiurl += APIKEY + FORMAT;
	return apiurl;
}

/**
 * Method to store items in localStorage (requires HTML5 browser support)
 * 
 * @param {Object} cacheName Name of the cachedObject
 * @param {Object} cacheMe Object to store in the cache
 */
function storeCache(cacheName, cacheMe)
{
	//create a new cache item
	var cacheItem = new Object();
	//record the date of creating this cache item
	cacheItem["cachedTime"] = new Date();
	//add the response to be cached
	cacheItem["cachedItem"] = cacheMe;
	//store it in the local storage
	localStorage.setItem("_cache_" + cacheName, JSON.stringify(cacheItem));
}

/**
 * Retrieve a cached item from local storage
 * 
 * @param {Object} cacheName Cached object to retrieve
 */
function getCache(cacheName)
{
	//get an item from the cache
	return JSON.parse(localStorage.getItem("_cache_" + cacheName));
}

/**
 * Method to check and see if a cached object is available and not expired
 * 
 * @param {Object} cacheName Name to check for a cached object
 * @param {Object} timeout Number of minutes before the cached object should be requested again
 */
function cacheExpired(cacheName, timeout)
{
	//set defaults if undefined
	timeout = timeout || GLOBALCACHETIMEOUT
	
	//attempt to get the item from the cache
	var cachedItem = getCache(cacheName);
	
	//if the item returned is null, then we've not made this request before
	if(cachedItem === null)
	{
		return true;
	}
	
	//now we check to see if the request os new enough to be refreshed or if we return from the cache
	var cachedTime = new Date(cachedItem["cachedTime"]);
	var cachedUntil = cachedTime.getTime() + timeout*60000;
	var now = new Date();
	if (now > cachedUntil)
	{
		return true;
	}
	return false;
}

/**
 * Give a relative time response instead of just the date objects
 * Credit to xtranophilist at https://github.com/xtranophilist/lastfm-widget/ for this function
 * 
 * @param {Object} time_value Date object to produce a relative time from
 */
function relativeTime(time_value) {
    var time_lt1min = 'less than 1 min ago';
    var time_1min = '1 min ago';
    var time_mins = '%1 mins ago';
    var time_1hour = '1 hour ago';
    var time_hours = '%1 hours ago';
    var time_1day = '1 day ago';
    var time_days = '%1 days ago';
    var parsed_date = Date.parse(time_value);
    var relative_to = (arguments.length > 1) ? arguments[1] : new Date();
    var delta = parseInt((relative_to.getTime() - parsed_date) / 1000);
    delta = delta + (relative_to.getTimezoneOffset() * 60);
    if (delta < 60) {
        return time_lt1min;
    } else if(delta < 120) {
        return time_1min;
    } else if(delta < (60*60)) {
        return time_mins.replace('%1', (parseInt(delta / 60)).toString());
    } else if(delta < (120*60)) {
        return time_1hour;
    } else if(delta < (24*60*60)) {
        return time_hours.replace('%1', (parseInt(delta / 3600)).toString());
    } else if(delta < (48*60*60)) {
        return time_1day;
    } else {
        return time_days.replace('%1', (parseInt(delta / 86400)).toString());
    }
}

//////////////////////////
// Recent Track Methods //
//////////////////////////

/**
 * Display the recent tracks for the user
 *  
 * @param {Object} userName name of the last.fm user
 * @param {Object} count number of tracks to get
 * @param {Object} tagid id of the html space to put the content
 */
function displayRecentTracks(userName, count)
{
	//display a loading sign
	displayLoadingContent();
	//parameters for our AJAX callback method
	var params = new Object();
	//url parameters for the api call
	var urlParams = new Object();
	//user to use
	urlParams["user"] = userName;
	//limit to set
	urlParams["limit"] = count;
	//api url to use for the request
	var apiurl = buildRequestURL("user.getrecenttracks", urlParams);
	//make the request and pass our ajax method to be executed when it is done
	request(apiurl, displayRecentTracksAJAX, params);
}

/**
 * AJAX call to parse the data and display it in the requested id field
 *  
 * @param {Object} params Parameters passed from the callback
 */
function displayRecentTracksAJAX(params)
{
	var response = params["response"];
	var htmlDisplay = "<table id='lfmwContentTable'>\n";
	for (index in response.recenttracks.track)
	{
	    try
	    {
	        var track = response["recenttracks"]["track"][index];
	        var artist = track["artist"]["#text"];
	        var album = track["album"]["#text"];
	        var trackName = track["name"];
            var trackURL = track["url"];
	        var variableDate = relativeTime(new Date(track["date"]["#text"]));
	        htmlDisplay += "<tr id='lfmwContentTableRow'>\n"
	        				+ "<td id='lfmwContentTableCell'>\n"
	        				+ "<a href='" + trackURL + "' target='_blank'>\n"
	        				+ "<span id='lfmwContentSpan'>" + trackName + "\n"
	        				+ "<span id='lfmwContentSpanHiddenRight'>&nbsp" + variableDate + "</span>\n"
	        				+ "</span>\n"
	        				+ "</a>\n"
	        				+ "</td>\n"
	        				+ "</tr>\n";
	    } catch (err) {}
	}
	htmlDisplay += "\n</table>";
	displayContent(htmlDisplay);
}

///////////////////////
// Top Track Methods //
///////////////////////

/**
 * Display the top tracks for the user
 *  
 * @param {Object} userName name of the last.fm user
 * @param {Object} count number of tracks to get
 * @param {Object} tagid id of the html space to put the content
 */
function displayTopTracks(userName, count)
{
	displayLoadingContent();
	var params = new Object();
	var urlParams = new Object();
	urlParams["user"] = userName;
	urlParams["limit"] = count;
	var apiurl = buildRequestURL("user.gettoptracks", urlParams);
	request(apiurl, displayTopTracksAJAX, params);
}

/**
 * AJAX call to parse the data and display it in the requested id field
 *  
 * @param {Object} params Parameters passed from the callback
 */
function displayTopTracksAJAX(params)
{
	var response = params["response"];
	var htmlDisplay = "<table id='lfmwContentTable'>\n";
	for (index in response.toptracks.track)
	{
	    try
	    {
	        var track = response["toptracks"]["track"][index];
	        var artist = track["artist"]["#text"];
	        var trackName = track["name"];
	        var trackPlayCount = track["playcount"];
            var trackURL = track["url"];
	        htmlDisplay += "<tr id='lfmwContentTableRow'>\n"
        				+ "<td id='lfmwContentTableCell'>\n"
        				+ "<a href='" + trackURL + "' target='_blank'>\n"
        				+ "<span id='lfmwContentSpan'>" + trackName + "\n"
        				+ "<span id='lfmwContentSpanDisplayRight'>&nbsp" + trackPlayCount + "&nbsp plays</span>\n"
        				+ "</span>\n"
        				+ "</a>\n"
        				+ "</td>\n"
        				+ "</tr>\n";
	    } catch (err) {}
	}
	htmlDisplay += "\n</table>";
	displayContent(htmlDisplay);
}

////////////////////////
// Top Artist Methods //
////////////////////////

/**
 * Display the top artists for the user
 *  
 * @param {Object} userName name of the last.fm user
 * @param {Object} count number of tracks to get
 * @param {Object} tagid id of the html space to put the content
 */
function displayTopArtists(userName, count)
{
	displayLoadingContent();
	var params = new Object();
	var urlParams = new Object();
	urlParams["user"] = userName;
	urlParams["limit"] = count;
	var apiurl = buildRequestURL("user.gettopartists", urlParams);
	request(apiurl, displayTopArtistsAJAX, params);
}

/**
 * AJAX call to parse the data and display it in the requested id field
 *  
 * @param {Object} params Parameters passed from the callback
 */
function displayTopArtistsAJAX(params)
{
	var response = params["response"];
	var htmlDisplay = "<table id='lfmwContentTable'>\n";
	for (index in response.topartists.artist)
	{
	    try
	    {
	        var artist = response["topartists"]["artist"][index];
	        var artistName = artist["name"];
	        var artistPlayCount = artist["playcount"];
            var artistURL = artist["url"];
	        htmlDisplay += "<tr id='lfmwContentTableRow'>\n"
        				+ "<td id='lfmwContentTableCell'>\n"
        				+ "<a href='" + artistURL + "' target='_blank'>\n"
        				+ "<span id='lfmwContentSpan'>" + artistName + "\n"
        				+ "<span id='lfmwContentSpanDisplayRight'>&nbsp" + artistPlayCount + "&nbsp plays</span>\n"
        				+ "</span>\n"
        				+ "</a>\n"
        				+ "</td>\n"
        				+ "</tr>\n";
	    } catch (err) {}
	}
	htmlDisplay += "\n</table>";
	displayContent(htmlDisplay);
}

////////////////////////////
// Recently Loved Methods //
////////////////////////////

/**
 * Display the recently loved tracks for the user
 *  
 * @param {Object} userName name of the last.fm user
 * @param {Object} count number of tracks to get
 * @param {Object} tagid id of the html space to put the content
 */
function displayRecentlyLoved(userName, count)
{
	displayLoadingContent();
	var params = new Object();
	var urlParams = new Object();
	urlParams["user"] = userName;
	urlParams["limit"] = count;
	var apiurl = buildRequestURL("user.getLovedTracks", urlParams);
	request(apiurl, displayRecentlyLovedAJAX, params);
}

/**
 * AJAX call to parse the data and display it in the requested id field
 *  
 * @param {Object} params Parameters passed from the callback
 */
function displayRecentlyLovedAJAX(params)
{
	var response = params["response"];
	var htmlDisplay = "<table id='lfmwContentTable'>\n";
	for (index in response.lovedtracks.track)
	{
	    try
	    {
	        var track = response["lovedtracks"]["track"][index];
	        var artist = track["artist"]["name"];
	        var trackName = track["name"];
	        var variableDate = relativeTime(new Date(track["date"]["#text"]));
            var trackURL = track["url"];
	        htmlDisplay += "<tr id='lfmwContentTableRow'>\n"
        				+ "<td id='lfmwContentTableCell'>\n"
        				+ "<a href='" + trackURL + "' target='_blank'>\n"
        				+ "<span id='lfmwContentSpan'>" + trackName + "\n"
        				+ "<span id='lfmwContentSpanDisplayRight'>&nbsp" + variableDate + "</span>\n"
        				+ "</span>\n"
        				+ "</a>\n"
        				+ "</td>\n"
        				+ "</tr>\n";
	    } catch (err) {}
	}
	htmlDisplay += "\n</table>";
	displayContent(htmlDisplay);
}

////////////////////////
// Top Album Methods //
////////////////////////

/**
 * Display the recently loved tracks for the user
 *  
 * @param {Object} userName name of the last.fm user
 * @param {Object} count number of tracks to get
 * @param {Object} tagid id of the html space to put the content
 */
function displayTopAlbums(userName, count)
{
	displayLoadingContent();
	var params = new Object();
	var urlParams = new Object();
	urlParams["user"] = userName;
	urlParams["limit"] = count;
	var apiurl = buildRequestURL("user.getTopAlbums", urlParams);
	request(apiurl, displayTopAlbumsAJAX, params);
}

/**
 * AJAX call to parse the data and display it in the requested id field
 *  
 * @param {Object} params Parameters passed from the callback
 */
function displayTopAlbumsAJAX(params)
{
	var response = params["response"];
	var htmlDisplay = "<table id='lfmwContentTable'>\n";
	for (index in response.topalbums.album)
	{
	    try
	    {
	        var album = response["topalbums"]["album"][index];
	        var artist = album["artist"]["name"];
	        var albumName = album["name"];
	        var albumPlayCount = album["playcount"];
            var albumURL = album["url"];
	        htmlDisplay += "<tr id='lfmwContentTableRow'>\n"
        				+ "<td id='lfmwContentTableCell'>\n"
        				+ "<a href='" + albumURL + "' target='_blank'>\n"
        				+ "<span id='lfmwContentSpan'>" + albumName + "\n"
        				+ "<span id='lfmwContentSpanDisplayRight'>&nbsp" + albumPlayCount + "&nbsp plays</span>\n"
        				+ "</span>\n"
        				+ "</a>\n"
        				+ "</td>\n"
        				+ "</tr>\n";
	    } catch (err) {}
	}
	htmlDisplay += "\n</table>";
	displayContent(htmlDisplay);
}